import getters from "@/store/getters"
import { PostState } from "@/types/StatePosts.type"
import { MOCKED_POSTS } from "./__mocks__/posts.mock"



describe('Getters tests', () => {

    describe('Getters for PostState', () => {

        let emptyState: PostState = {
            posts: []
        }

        let postsState: PostState = {
            posts: MOCKED_POSTS
        }


        it('Get all General Posts', () => {

            const generalPosts = getters.getAllGeneralPosts(postsState)

            console.log('GENERAL POSTS', generalPosts);

            // Check obtained length of General posts
            expect(generalPosts.length).toBe(3);
            // Check what posts are been obtianed
            expect(generalPosts).toEqual([MOCKED_POSTS[0],MOCKED_POSTS[1],MOCKED_POSTS[2]]);

        });

        it('Get all Sports Posts', () => {

            const sportsPosts = getters.getAllSportsPosts(postsState)

            console.log('SPORTS POSTS', sportsPosts);

            // Check obtained length of General posts
            expect(sportsPosts.length).toBe(1);
            // Check what posts are been obtianed
            expect(sportsPosts).toEqual([MOCKED_POSTS[3]]);

        });

        it('Get all IT Posts', () => {

            const itPosts = getters.getAllITPosts(postsState)

            console.log('IT POSTS', itPosts);

            // Check obtained length of General posts
            expect(itPosts.length).toBe(2);
            // Check what posts are been obtianed
            expect(itPosts).toEqual([MOCKED_POSTS[4], MOCKED_POSTS[5]]);

        });

        it('Get Posts by title', () => {

            const posts = [MOCKED_POSTS[0], MOCKED_POSTS[1]];

            const receivedPost = getters.getPostByTitle(postsState, { posts })('My Second General Post');

            // Check what posts are been obtianed
            expect(receivedPost).toEqual([MOCKED_POSTS[1]]);

        });

    })




})