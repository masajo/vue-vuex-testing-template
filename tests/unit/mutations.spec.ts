import mutations from "@/store/mutations";
import { Post, PostTypes } from "@/types/Post.type";
import { PostState } from "@/types/StatePosts.type";


describe('Mutation Tests', () => {

    describe('SET_POST', () => {

        let postState: PostState = {
            posts: []
        }
    
        beforeEach(() => {

            // CONTEXT:
            postState = {
                posts: []
            }
        })
        
        it('Adds correctly a Post to PostState', () => {

            // We Create a new post to include in the list of posts of PostsState
            const newPost: Post = {
                id: 1,
                title: 'My first Post',
                type: PostTypes.GENERAL
            }

            // ACT: We call the SET_POST mutation of Store 
            mutations.SET_POST(postState,newPost);

            console.log('Updated State', postState);

            // Assertions
            expect(postState).toEqual({
                 posts: [ newPost ]
            });

        });

    });






});