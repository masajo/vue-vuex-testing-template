import actions from '@/store/actions';

import { AutheticationData } from '@/types/AutheticationData.type';


let url: string = '';
let body: AutheticationData | undefined;
let mockError: boolean = false;


jest.mock('axios', () => (
    {
        post: (_url: string, _body: AutheticationData) => {
            
            return new Promise((resolve, reject) => {

                if(mockError){
                    reject('Forced Mock error');
                }

                url = _url;
                body = _body;
                resolve(
                    {
                        data: {
                            token: 'abcdef'
                        }
                    }
                )
            })

        }

    }
))


describe('Authetication tests ', () => {

    it('Login successful', async () => {

        // CONTEXT:
        // Mocking function
        const commit = jest.fn();

        // Stub de Autheticacion Data
        const autheticationData: AutheticationData = {
            email: 'eve.holt@reqres.in',
            password: 'abcdef'
        }

        // ACT:
        await actions.authenticate(commit, autheticationData);

        // ASSERTS:
        await expect(url).toBe('https://reqres.in/api/login');
        await expect(body).toStrictEqual(autheticationData);
        await expect(commit).toHaveBeenCalledWith('SET_TOKEN', 'abcdef');

    });

    // TODO: Resolve Test Error
    xit('Catches an error', async () => {

        const commit = jest.fn();

        mockError = true;

        // Stub de Autheticacion Data
        const autheticationData: AutheticationData = {
            email: 'eve.holt@reqres.in',
            password: 'abcdef'
        }

        await expect(actions.authenticate(commit, autheticationData)).rejects.toThrowError();

    })


})





