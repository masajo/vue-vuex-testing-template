import { Post, PostTypes } from "@/types/Post.type";

/**
 * 3 General posts
 * 1 Sports post
 * 2 IT posts
 */
export const MOCKED_POSTS: Post[] = [
    {
        id: 1,
        title: 'My First General Post',
        type: PostTypes.GENERAL
    },
    {
        id: 2,
        title: 'My Second General Post',
        type: PostTypes.GENERAL
    },
    {
        id: 3,
        title: 'My Third General Post',
        type: PostTypes.GENERAL
    },
    {
        id: 4,
        title: 'My First Sports Post',
        type: PostTypes.SPORT
    },
    {
        id: 5,
        title: 'My First IT Post',
        type: PostTypes.IT
    },
    {
        id: 6,
        title: 'My Second IT Post',
        type: PostTypes.IT
    }

]