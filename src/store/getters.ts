import { PostTypes } from "@/types/Post.type";
import { PostState } from "@/types/StatePosts.type";
import { PostGetters } from '@/types/PostGetters.type';


export default {

    getAllGeneralPosts: (state: PostState) => {
        return state.posts.filter(post => post.type === PostTypes.GENERAL);
    },

    getAllSportsPosts: (state: PostState) => {
        return state.posts.filter(post => post.type === PostTypes.SPORT);
    },

    getAllITPosts: (state: PostState) => {
        return state.posts.filter(post => post.type === PostTypes.IT);
    },

    getPostByTitle: (state: PostState, getters: PostGetters) => (title: string) => {
        return getters.posts.filter(post => post.title === title)
    }

}