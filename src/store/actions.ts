import { AutheticationData } from "@/types/AutheticationData.type";
import axios, { AxiosResponse } from "axios";

export default {
    authenticate: (commit: (action:string, payload:string) => void, { email, password }: AutheticationData ) => {
        try {

            const autheticated = axios.post('https://reqres.in/api/login', { email, password });

            autheticated
                .then((response: AxiosResponse) => {
                    if(response.data.token){
                        let token = response.data.token;
                        commit('SET_TOKEN', token);
                    }
                }).catch((error: any) => {
                    throw Error('Error in API LOGIN');
                }).finally(() => console.info('Login Request Ended'));

        } catch (error) {
            throw Error('Error in API LOGIN');
        }
    }
}