import { Post } from "@/types/Post.type";
import { PostState } from "@/types/StatePosts.type";

export default {
    SET_POST(state: PostState, post: Post){
        state.posts.push(post); // Add the post to the store
    }
}
