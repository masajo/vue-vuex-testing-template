import { PostState } from '@/types/StatePosts.type'
import { createStore } from 'vuex'
import mutations from './mutations'

// Creation of initial PostState
const initialPostState: PostState = {
  posts: []
}

export default createStore({
  state: initialPostState ,
  getters: {
  },
  mutations: mutations,
  actions: {
  },
  modules: {
  }
})
