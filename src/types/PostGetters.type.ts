import { Post } from "./Post.type";

export type PostGetters = {
    posts: Post[]
}