import { Post } from "./Post.type";

/**
 * Export the type of PostState that will be stored in the Vuex Store
 */

export type PostState = {
    posts: Post[]
}