export enum PostTypes {
    GENERAL = 'GENERAL',
    SPORT = 'SPORT',
    IT = 'IT'
}


export type Post = {
    id: number,
    title: string,
    type: PostTypes,
}